<?php

/**
 * Implements hook_menu().
 */
function content_type_categories_menu() {

  $items['admin/config/content/content-type-categories'] = array(
    'title' => 'Content Type Categories',
    'description' => 'Organize/group content types',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('content_type_categories_config_form'),
    'access arguments' => array('administer content types'),
  );
  return $items;
}

/**
 * Implements hook_menu_alter().
 */
function content_type_categories_menu_alter(&$items) {
  $items['node/add']['page callback'] = 'content_type_categories_type_list';
}

/**
 * Implements hook_form_node_type_alter().
 * Adds category field to content type
 */
function content_type_categories_form_node_type_form_alter(&$form, &$form_state, $form_id) {
  $options = array();
  $categories = variable_get('content_type_categories' , '');
  if (isset($categories)) {
    $categories = preg_split('/$\R?^/m', $categories);
    foreach ($categories as $category) {
      $options[trim($category)] = trim($category);
    }
    ksort($options);
  }
  array_unshift($options, t('None'));
  $form['type_category']['#collapsible'] = TRUE;
  $form['type_category']['#description'] = 'Set the category for this content type. ' . l('Manage categories', 'admin/config/content/content-type-categories');
  $form['type_category']['#group'] = 'additional_settings';
  $form['type_category']['#title'] = 'Category';
  $form['type_category']['#type'] = 'fieldset';
  $form['type_category']['content_type_category'] = array(
    '#type' => 'radios',
    '#title' => t('Category'),
    '#default_value' => trim(variable_get('content_type_category_' . $form['#node_type']->type, 'none')),
    '#options' => $options,
    '#validated' => TRUE,
  );

}

function content_type_categories_type_list() {
  $categories = array();
  
  // Query all content types
  $result = db_query('SELECT * FROM {node_type}')->fetchAllAssoc('type');
  
  foreach ($result as $type) { 
    // Check to see if content type has a category set
    // If it does add it to the categories array and remove it from the results array
    $category = variable_get('content_type_category_' . $type->type, '');
    if ($category) { 
      $categories[$category][$type->name] = $type; 
      unset($result[$type->type]);
    }
  }
  
  $output = '';
  
  // List categories first
  ksort($categories);
  foreach ($categories as $title => $category) {
    $output .= '<div class="content-type-category"><h2>' . $title . '</h2>';
    $output .= "<ul class='admin-list'>";
    ksort($category);
    foreach ($category as $content_type) {
      $item['localized_options']['html'] = TRUE;
      $item['href'] = 'node/add/' . str_replace('_', '-', $content_type->type);
      $item['title'] = '<span class="icon"></span>' . filter_xss_admin($content_type->name);
      $output .= "<li>";
      $output .= l($item['title'], $item['href'], $item['localized_options']);
      $output .= '<div class="description">'. filter_xss_admin($content_type->description) .'</div>';
      $output .= "</li>";
    }
    $output .= '</ul>';
    $output .='</div>';
  }
  
  // List remaining content types
  if (!empty($result)) {
    $output .= '<div class="content-type-category"><h2>' . t('Other Content Types') . '</h2>';
    $output .= "<ul class='admin-list'>";
    foreach ($result as $content_type) {
      $item['localized_options']['html'] = TRUE;
      $item['href'] = 'node/add/' . str_replace('_', '-', $content_type->type);
      $item['title'] = '<span class="icon"></span>' . filter_xss_admin($content_type->name);
      $output .= "<li>";
      $output .= l($item['title'], $item['href'], $item['localized_options']);
      $output .= '<div class="description">'. filter_xss_admin($content_type->description) .'</div>';
      $output .= "</li>";
    }
    $output .= '</ul>';
    $output .='</div>';
  }
  return $output;
}

function content_type_categories_config_form($form, &$form_state) {
  $form = array();
  $form['content_type_categories'] = array(
    '#title' => t('Categories'),
    '#type' => 'textarea',
    '#description' => t('List content type categories one per line.'),
    '#default_value' => variable_get('content_type_categories' , ''),
  );
  $form['button_shortcode_options']['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Save Categories'),
  );
  
  
  $form['#submit'][] = 'content_type_categories_config_submit';
  return $form;
}

function content_type_categories_config_submit($form, &$form_state) {
  variable_set('content_type_categories', $form_state['values']['content_type_categories']);
  drupal_set_message(t('Content Type Categories have been updated.'));
}